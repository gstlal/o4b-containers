# o4b-containers

Analysis containers to define production environment for GstLAL low-latency online analyses in O4b.

## containers

Containers are built automatically by the CI pipeline and pushed to the container registry at containers.ligo.org. Pull containers using:
```bash
singularity build <build-name> docker://containers.ligo.org/gstlal/o4b-containers:main
```

To make a writable container, using: 
```
singularity build --sandbox --fix-perms <build-name> docker://containers.ligo.org/gstlal/o4b-containers:main
```



