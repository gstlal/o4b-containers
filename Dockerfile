FROM containers.ligo.org/lscsoft/gstlal:o4b-online

ARG gitlab_token
ARG gitlab_name

LABEL name "gstlal o4b MDC online analysis container" \
      date="2023-11-09" 

# ADD config files needed to launch an online analysis from scratch
ADD config.yml online-analysis/config.yml
ADD web/inspiral.yml online-analysis/web/inspiral.yml
ADD profiles/icds_jacob_online.yml online-analysis/profiles/icds_jacob_online.yml
ADD influx_creds.sh.sample online-analysis/influx_creds.sh

# set up directories that will be needed
RUN mkdir -p online-analysis/bank/
RUN mkdir -p online-analysis/psd/
RUN mkdir -p online-analysis/mass_model/

# ADD any patches we need to apply manually
ADD patches/snglcoinc.patch patches/snglcoinc.patch

USER root

RUN mkdir -p src && cd src && \
    git clone https://$gitlab_name:$gitlab_token@git.ligo.org/gstlal/pastro.git && \
    cd pastro && \
    git checkout o4a-online && \
    python3 setup.py install --old-and-unmanageable && \
    cd ../../

RUN mkdir -p src && cd src && \
    git clone https://$gitlab_name:$gitlab_token@git.ligo.org/chad-hanna/manifold.git && \
    cd manifold && \
    git checkout 0.0.9 && \
    python3 setup.py install --old-and-unmanageable && \
    cd ../../

RUN mkdir -p src && cd src && \
    git clone https://$gitlab_name:$gitlab_token@git.ligo.org/lscsoft/lalsuite.git && \
    cd lalsuite && \
    git checkout lalsuite-v7.13 && \
    git apply ../../patches/snglcoinc.patch && \
    ./00boot && \
    ./configure --prefix=/usr --libdir=/usr/lib64 --enable-swig-python --disable-gcc-flags && \
    make && \
    make install -j10

ENTRYPOINT bash
