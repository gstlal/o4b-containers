#!/bin/bash


# Author: Shomik Adhicary
# Code to ssh into the shared accounts, get to the analysis directories, remove and check for counts

# Parse command-line arguments
if [ $# -ne 1 ]; then
	    echo "Usage: $0 <Spcify gpstime to remove counts for>"
	        exit 1
fi

TIME="$1"


# ssh Hosts
HOST_CIT="gstlalcbc.online@gstlal.ligo.caltech.edu"
HOST_PSU="gstlalcbc@ligo-hd-01.gwave.ics.psu.edu"
HOST_UWM="gstlalcbc.online@submit.ligo.uwm.edu"


# Root Directories Name
MAIN="~/observing/4/b/runs/trigs."
MAIN_ALICE_FFT8="~/observing/4/dev/b/runs/trigs."
MAIN_ESME="~/observing/4/pre_engineering/runs/trigs."


# Analyses Names
ALICE="alice_o4b"
ALICE_FFT8="alice_o4b_fft8"
BOB="bob_o4b"
CHARLIE_CIT="charlie-cit_o4b"
CHARLIE_ICDS="charlie-icds_o4b"
EDWARD="edward_o4b"
JACOB="jacob_o4b"
RICK="rick_o4b"
ESME="esme_240301"


# Cluster specific analysis list
ANALYSES_CIT=("ALICE" "ALICE_FFT8" "CHARLIE_CIT" "EDWARD")
ANALYSES_PSU=("CHARLIE_ICDS" "JACOB" "RICK" "ESME")
ANALYSES_UWM=("BOB")


# Function to change directory to the analysis
change_directory_to_analysis() {
	local main_path="$1"
	local analysis="$2"
	local analysis_path="$main_path$analysis"
	echo "Changing directory to $analysis_path..."
	cd "$analysis_path" || { echo "Failed to change directory to $analysis_path"; return 1; }
}


# Function to obtain singularity container from config.yml
get_singularity_container() {
	local singularity_path
	singularity_path=$(grep image config.yml | awk -F': ' '{print $2}' | xargs)
	echo "$singularity_path"
}


# Count Tracker Function
count_tracker() {
	local singularity_path="$1"
	local gps_time="$2"
	
	# Remove Counts
	echo "Removing counts..."
	singularity exec "$singularity_path" gstlal_ll_inspiral_remove_counts --action remove --gps-time "$gps_time" *noninj*

	# Check Counts
	echo "Checking whether counts are removed..."
	singularity exec "$singularity_path" gstlal_ll_inspiral_remove_counts --action check --gps-time "$gps_time" *noninj*
}


# Function to process time specified
process_time() {
	local main_path="$1"
	local analysis="$2"
	local time="$3"

	change_directory_to_analysis "$main_path" "$analysis"

	echo "Obtaining container..."
	local singularity_path=$(get_singularity_container)

	count_tracker "$singularity_path" "$time"
}


# Loop through each host and perform SSH operation
for HOST in "$HOST_CIT" "$HOST_PSU" "$HOST_UWM"; do

	echo "Connecting to $HOST..."
	ssh -i .ssh/ligo "$HOST"  "

		echo "Logged into $HOST";
		echo ""

		# Define functions in remote server
		$(declare -f change_directory_to_analysis)
		$(declare -f get_singularity_container)
		$(declare -f count_tracker)
		$(declare -f process_time)

		# CIT
		if [ \"$HOST\" == \"$HOST_CIT\" ]; then
			for ANALYSIS in \"$CHARLIE_CIT\"; do
				process_time "$MAIN" "\$ANALYSIS" "$TIME"
				echo ""
			done;
			for ANALYSIS in \"$ALICE_FFT8\"; do
				process_time "$MAIN_ALICE_FFT8" "\$ANALYSIS" "$TIME"
				echo ""
			done;
		fi
		
		# PSU
		if [ \"$HOST\" == \"$HOST_PSU\" ]; then
			for ANALYSIS in \"$CHARLIE_ICDS\" \"$RICK\"; do
				process_time "$MAIN" "\$ANALYSIS" "$TIME"
				echo ""
			done;
			for ANALYSIS in \"$ESME\"; do
				process_time "$MAIN_ESME" "\$ANALYSIS" "$TIME"
				echo ""
			done;
		fi

		# UWM
		if [ \"$HOST\" == \"$HOST_UWM\" ]; then
			for ANALYSIS in \"$BOB\"; do
				process_time "$MAIN" "\$ANALYSIS" "$TIME"
				echo ""
			done;
		fi

	"
	echo ""
done

